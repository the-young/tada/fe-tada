import React from 'react'
import { View, Text } from 'react-native'
import { Stack } from './create-navigator'
import { TransitionPresets } from '@react-navigation/stack'
// import {Tr } from '@react-navigation/core'
const RootNavigation = () => {
    return (
        <Stack.Navigator initialRouteName="Home" screenOptions={{
            ...TransitionPresets.SlideFromRightIOS,
            headerTitleAlign: 'center',
        }}>
            <Stack.Screen name="Home" children={({ navigation }) => <View><Text onPress={() => navigation.navigate('Homex')}>xxxx</Text></View>} />
            <Stack.Screen name="Homex" children={() => <View><Text>Homex x v</Text></View>} />
        </Stack.Navigator>
    )
}

export default RootNavigation

